/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dashboard;

import database.DatabaseConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author test
 */
public class Master extends javax.swing.JInternalFrame {

    /**
     * Creates new form Master
     */
    Connection koneksi;
    DefaultTableModel dtm;
    int baris;
    
    public Master() {
        initComponents();
        setVisible(true);
        koneksi = DatabaseConnection.getKoneksi();
        BtnUpdate.setEnabled(false);
        BtnCancel.setEnabled(false);
        BtnDelete.setEnabled(false);
        listPengguna();
        listPelanggan();
        listTarif();
        gJ();
    }
    
    public void gJ(){
        try {
            String query = "SELECT * FROM t_tarif";
            Statement stmt = koneksi.createStatement();
            ResultSet rs = stmt.executeQuery(query);
             
            while (rs.next()) {                
                cbKdTarif.addItem(rs.getString("kd_tarif"));
            }
             
            rs.last();
            int jumlahdata = rs.getRow();
            rs.first();
             
        } catch (SQLException e) {
        }
    }
    
    private void addPelanggan(){
            String Id = txtIdPel.getText().toString();
            String NoMeter = txtNo.getText().toString();
            String NamaPel = txtNama.getText().toString();
            String AlamatPel = txtAlamat.getText().toString();
            String KodeTarif = cbKdTarif.getSelectedItem().toString();
        
        try{
            Statement stmt = koneksi.createStatement();
            String query = "INSERT INTO t_pelanggan VALUES('"+ Id + "', '" + NoMeter + "', "
                    + "'" + NamaPel + "' , '" + AlamatPel + "', '" + KodeTarif + "') ";
                    System.out.println("" + query);
            int success = stmt.executeUpdate(query);
            if(success == 1){
                JOptionPane.showMessageDialog(null, "Data Berhasil Ditambahkan");
                listPengguna();
            }else{
                JOptionPane.showMessageDialog(null, "Data Gagal Ditambahkan");
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan Pada Database");
        }
    }
    
    private void listPelanggan(){
        String[] kolom = {"Id Pelanggan", "No Meter", "Nama", "Alamat", "Kode Tarif"};
        dtm = new DefaultTableModel (null, kolom);
        try{
            Statement stmt = koneksi.createStatement();
            String query = "SELECT * FROM t_pelanggan";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()){
                String id = rs.getString("id_pelanggan");
                String no = rs.getString("no_meter");
                String nama= rs.getString("nama");
                String alamat = rs.getString("alamat");
                String kd = rs.getString("kd_tarif");

                dtm.addRow(new String[] {id, no, nama, alamat, kd});
            }
            tblPel.setModel(dtm);
        }catch(SQLException e){
            System.out.println("" + e);
        }
    }

    private void editPelanggan() {
        String EditedId = tblPel.getValueAt(baris, 0).toString();
        String Id = txtIdPel.getText().toString();
        String NoMeter = txtNo.getText().toString();
        String NamaPel = txtNama.getText().toString();
        String AlamatPel = txtAlamat.getText().toString();
        String KodeTarif = cbKdTarif.getSelectedItem().toString();
        
        try{
            Statement stmt = koneksi.createStatement();
            String sql = "UPDATE t_pengguna SET "
                    + "id_pelanggan = '" + Id + "', "
                    + "no_meter = '" + NoMeter + "', "
                    + "nama = '" + NamaPel + "', "
                    + "alamat = '" + AlamatPel + "', "
                    + "kd_tarif = '" + KodeTarif + "',"
                    + "WHERE id_pelanggan = '" + EditedId + "' ";
            int berhasil = stmt.executeUpdate(sql);
            if (berhasil == 1){
                JOptionPane.showMessageDialog(null, "Data Berhasil Diupdate");
                txtIdPel.setText("");
                txtNo.setText("");
                txtNama.setText("");
                txtAlamat.setText("");
                cbKdTarif.setSelectedItem("");
                btnUpdatePel.setEnabled(false);
                btnCancelPel.setEnabled(false);
                btnDeletePel.setEnabled(false);
                listPelanggan();
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Diupdate");
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
    private void deletePelanggan() {
        String deletedId = tblPel.getValueAt(baris, 0).toString();
        try {
            Statement stmt = koneksi.createStatement();
            String query = "DELETE FROM t_pelanggan WHERE id_pelanggan = '"+deletedId+"'; ";
            int berhasil = stmt.executeUpdate(query);
            if(berhasil == 1)
            {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
                dtm.getDataVector().removeAllElements();
                listPengguna();
            }
            else {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
            }
        }
        catch(SQLException ex) {
                        ex.printStackTrace();
        }
    }
    
    private void addPengguna(){
        String IdPengguna = TvID.getText();
        String NamaPengguna = TvNama.getText();
        String email = TvEmail.getText();
        String Nohp = TvNoHp.getText();
        String Username = TvUsername.getText();
        String Password = new String(TvPassword.getPassword());
        String HakAkses = CbHak.getSelectedItem().toString();
        
        try{
            Statement stmt = koneksi.createStatement();
            String query = "INSERT INTO t_pengguna VALUES ('"
                    +IdPengguna+"','"
                    +NamaPengguna+"','"
                    +Username+"','"
                    +Password+"','"
                    +email+"','"
                    +Nohp+"','"
                    +HakAkses+"')";
            int success = stmt.executeUpdate(query);
            if(success == 1){
                JOptionPane.showMessageDialog(null, "Data Berhasil Ditambahkan");
                listPengguna();
            }else{
                JOptionPane.showMessageDialog(null, "Data Gagal Ditambahkan");
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan Pada Database");
        }
    }
    
    private void listPengguna(){
        String[] kolom = {"id", "nama", "email", "nohp", "username", "password", "hakakses"};
        
        dtm = new DefaultTableModel (null, kolom);
        try{
            Statement stmt = koneksi.createStatement();
            String query = "SELECT * from t_pengguna" ;
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()){
                String id       = rs.getString("id_pengguna");
                String nama     = rs.getString("nama");
                String email   = rs.getString("email");
                String nohp       = rs.getString("no_hp");
                String username       = rs.getString("username");
                String password       = rs.getString("password");
                String hakakses       = rs.getString("hak_akses");
                
                dtm.addRow(new String[]{id, nama, email, nohp, username, password, hakakses});
            }
        }
        catch (SQLException ex){
            ex.printStackTrace();
        }
        tbl_pengguna.setModel(dtm);
    }

    private void editPengguna() {
        String EditedId = tbl_pengguna.getValueAt(baris, 0).toString();
        String NamaPengguna = TvNama.getText();
        String email = TvEmail.getText();
        String Nohp = TvNoHp.getText();
        String Username = TvUsername.getText();
        String Password = TvPassword.getPassword().toString();
        String HakAkses = CbHak.getSelectedItem().toString();
        
        try{
            Statement stmt = koneksi.createStatement();
            String sql = "UPDATE t_pengguna SET "
                    + "nama = '" + NamaPengguna + "', "
                    + "username = '" + Username + "', "
                    + "password = '" + Password + "', "
                    + "email = '" + email + "', "
                    + "no_hp = '" + Nohp + "',"
                    + "hak_akses = '" + HakAkses + "'"
                    + "WHERE id_pengguna = '" + EditedId + "' ";
            int berhasil = stmt.executeUpdate(sql);
            if (berhasil == 1){
                JOptionPane.showMessageDialog(null, "Data Berhasil Diupdate");
                TvID.setText("");
                TvNama.setText("");
                TvEmail.setText("");
                TvNoHp.setText("");
                TvUsername.setText("");
                TvPassword.setText("");
                CbHak.setSelectedItem("");
                BtnUpdate.setEnabled(false);
                BtnCancel.setEnabled(false);
                BtnDelete.setEnabled(false);
                listPengguna();
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Diupdate");
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
    private void deletePengguna() {
        String deletedId = tbl_pengguna.getValueAt(baris, 0).toString();
        try {
            Statement stmt = koneksi.createStatement();
            String query = "DELETE FROM t_pengguna WHERE id_pengguna = '"+deletedId+"'; ";
            int berhasil = stmt.executeUpdate(query);
            if(berhasil == 1)
            {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
                dtm.getDataVector().removeAllElements();
                listPengguna();
            }
            else {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
            }
        }
        catch(SQLException ex) {
                        ex.printStackTrace();
        }
    }
    
    private void addTarif(){
            String KdTarif = txtKd.getText().toString();
            String Daya = txtDaya.getText().toString();
            String Kwh = txtKwh.getText().toString();
            String Denda = txtDenda.getText().toString();
        
        try{
            Statement stmt = koneksi.createStatement();
            String query = "INSERT INTO t_tarif VALUES('"+ KdTarif + "', '" + Daya + "', "
                    + "'" + Kwh + "', '" + Denda + "') ";
                    System.out.println("" + query);
            int success = stmt.executeUpdate(query);
            if(success == 1){
                JOptionPane.showMessageDialog(null, "Data Berhasil Ditambahkan");
                listPengguna();
            }else{
                JOptionPane.showMessageDialog(null, "Data Gagal Ditambahkan");
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan Pada Database");
        }
    }
    
    private void listTarif(){
        String[] kolom = {"Kode Tarif", "Daya", "Tarif/kwh", "Denda"};
        dtm = new DefaultTableModel (null, kolom);
        try{
            Statement stmt = koneksi.createStatement();
            String query = "SELECT * FROM t_tarif";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()){
                String kd = rs.getString("kd_tarif");
                String daya = rs.getString("daya");
                String kwh = rs.getString("tarif");
                String denda = rs.getString("denda");

                dtm.addRow(new String[] {kd, daya, kwh, denda});
            }
            tblTarif.setModel(dtm);
        }catch(SQLException e){
            System.out.println("" + e);
        }
    }

    private void editTarif() {
        String EditedId = tblPel.getValueAt(baris, 0).toString();
        String KdTarif = txtKd.getText().toString();
            String Daya = txtDaya.getText().toString();
            String Kwh = txtKwh.getText().toString();
            String Denda = txtDenda.getText().toString();
        
        try{
            Statement stmt = koneksi.createStatement();
            String sql = "UPDATE t_tarif SET kd_tarif='" + KdTarif + "' "
                            + ", daya='" + Daya + "', tarif='" + Kwh + "'"
                            + ", denda='" + Denda + "' WHERE kd_tarif = '"+EditedId+"'";
            int berhasil = stmt.executeUpdate(sql);
            if (berhasil == 1){
                JOptionPane.showMessageDialog(null, "Data Berhasil Diupdate");
                txtKd.setText("");
                txtDaya.setText("");
                txtKwh.setText("");
                txtDenda.setText("");
                btnUpdate.setEnabled(false);
                btnCancel.setEnabled(false);
                btnDelete.setEnabled(false);
                listTarif();
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Diupdate");
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
    private void deleteTarif() {
        String deletedId = tblTarif.getValueAt(baris, 0).toString();
        try {
            Statement stmt = koneksi.createStatement();
            String query = "DELETE FROM t_tarif WHERE kd_tarif = '"+deletedId+"'; ";
            int berhasil = stmt.executeUpdate(query);
            if(berhasil == 1)
            {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
                dtm.getDataVector().removeAllElements();
                listPengguna();
            }
            else {
                JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
            }
        }
        catch(SQLException ex) {
                        ex.printStackTrace();
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtIdPel = new java.awt.TextField();
        txtNo = new java.awt.TextField();
        txtNama = new java.awt.TextField();
        txtAlamat = new java.awt.TextArea();
        cbKdTarif = new javax.swing.JComboBox<>();
        btnInsertPel = new java.awt.Button();
        btnUpdatePel = new java.awt.Button();
        btnDeletePel = new java.awt.Button();
        btnCancelPel = new java.awt.Button();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPel = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        txtCariPel = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        CbHak = new javax.swing.JComboBox<>();
        TvUsername = new java.awt.TextField();
        jLabel10 = new javax.swing.JLabel();
        TvNama = new java.awt.TextField();
        jLabel11 = new javax.swing.JLabel();
        TvEmail = new java.awt.TextField();
        jLabel16 = new javax.swing.JLabel();
        TvNoHp = new java.awt.TextField();
        TvPassword = new javax.swing.JPasswordField();
        TvID = new java.awt.TextField();
        jLabel17 = new javax.swing.JLabel();
        BtnUpdate = new java.awt.Button();
        BtnCancel = new java.awt.Button();
        BtnDelete = new java.awt.Button();
        BtnInsert = new java.awt.Button();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbl_pengguna = new javax.swing.JTable();
        jPanel11 = new javax.swing.JPanel();
        TvSearch = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtKd = new java.awt.TextField();
        txtDaya = new java.awt.TextField();
        txtKwh = new java.awt.TextField();
        jLabel19 = new javax.swing.JLabel();
        txtDenda = new javax.swing.JTextField();
        btnInsert = new java.awt.Button();
        btnUpdate = new java.awt.Button();
        btnDelete = new java.awt.Button();
        btnCancel = new java.awt.Button();
        jPanel16 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblTarif = new javax.swing.JTable();
        jPanel17 = new javax.swing.JPanel();
        txtCariTarif = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();

        jButton1.setText("jButton1");

        setPreferredSize(new java.awt.Dimension(680, 580));

        jTabbedPane1.setPreferredSize(new java.awt.Dimension(680, 580));

        jLabel2.setText("ID Pelanggan");

        jLabel3.setText("No. Meter");

        jLabel4.setText("Nama");

        jLabel5.setText("Alamat");

        jLabel6.setText("Kode Tarif");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtNo, javax.swing.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)
                    .addComponent(txtIdPel, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNama, javax.swing.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)
                    .addComponent(txtAlamat, javax.swing.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)
                    .addComponent(cbKdTarif, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(113, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(txtIdPel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(txtNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(txtNama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(txtAlamat, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cbKdTarif, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        btnInsertPel.setLabel("Insert");
        btnInsertPel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertPelActionPerformed(evt);
            }
        });

        btnUpdatePel.setLabel("Update");
        btnUpdatePel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdatePelActionPerformed(evt);
            }
        });

        btnDeletePel.setLabel("Delete");
        btnDeletePel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeletePelActionPerformed(evt);
            }
        });

        btnCancelPel.setLabel("Cancel");
        btnCancelPel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelPelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnUpdatePel, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnInsertPel, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDeletePel, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelPel, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnInsertPel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnUpdatePel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDeletePel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelPel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 11, Short.MAX_VALUE))
        );

        tblPel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "ID Pelanggan", "No. Meter", "Nama", "Alamat", "Kode tarif"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblPel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblPelMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblPel);
        if (tblPel.getColumnModel().getColumnCount() > 0) {
            tblPel.getColumnModel().getColumn(0).setResizable(false);
            tblPel.getColumnModel().getColumn(1).setResizable(false);
            tblPel.getColumnModel().getColumn(2).setResizable(false);
            tblPel.getColumnModel().getColumn(3).setResizable(false);
            tblPel.getColumnModel().getColumn(3).setHeaderValue("Alamat");
            tblPel.getColumnModel().getColumn(4).setResizable(false);
            tblPel.getColumnModel().getColumn(4).setHeaderValue("Kode tarif");
        }

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 812, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(86, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(153, 153, 153));

        txtCariPel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCariPelActionPerformed(evt);
            }
        });
        txtCariPel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCariPelKeyReleased(evt);
            }
        });

        jLabel1.setText("Search");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtCariPel, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCariPel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(114, 114, 114))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        jTabbedPane1.addTab("Pelanggan", jPanel1);

        jPanel9.setBackground(new java.awt.Color(153, 153, 153));

        jLabel7.setText("Username");

        jLabel8.setText("Password");

        jLabel9.setText("Hak Akses");

        CbHak.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Petugas", "Admin" }));

        jLabel10.setText("Nama");

        jLabel11.setText("Email");

        jLabel16.setText("No. Hp");

        TvID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TvIDActionPerformed(evt);
            }
        });

        jLabel17.setText("ID Pengguna");

        BtnUpdate.setLabel("Update");
        BtnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnUpdateActionPerformed(evt);
            }
        });

        BtnCancel.setLabel("Cancel");
        BtnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelActionPerformed(evt);
            }
        });

        BtnDelete.setLabel("Delete");
        BtnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDeleteActionPerformed(evt);
            }
        });

        BtnInsert.setLabel("Insert");
        BtnInsert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnInsertActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)
                            .addComponent(jLabel16)
                            .addComponent(jLabel17))
                        .addGap(26, 26, 26)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TvID, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(TvEmail, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                                .addComponent(TvNama, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(TvNoHp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel7)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel9)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(CbHak, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(TvUsername, javax.swing.GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE)
                                .addComponent(TvPassword)))
                        .addGap(0, 218, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(BtnInsert, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BtnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BtnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BtnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TvUsername, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(TvPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CbHak, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9))
                        .addGap(23, 23, 23))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addComponent(TvID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(TvNama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(TvEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(TvNoHp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(36, 36, 36)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnInsert, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24))
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 132, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, 231, Short.MAX_VALUE)
        );

        tbl_pengguna.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Id Pengguna", "Nama", "Email", "No.Hp", "Username", "Password", "Hak Akses"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_pengguna.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_penggunaMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tbl_pengguna);
        if (tbl_pengguna.getColumnModel().getColumnCount() > 0) {
            tbl_pengguna.getColumnModel().getColumn(0).setResizable(false);
            tbl_pengguna.getColumnModel().getColumn(1).setResizable(false);
            tbl_pengguna.getColumnModel().getColumn(2).setResizable(false);
            tbl_pengguna.getColumnModel().getColumn(3).setResizable(false);
            tbl_pengguna.getColumnModel().getColumn(4).setResizable(false);
            tbl_pengguna.getColumnModel().getColumn(5).setResizable(false);
            tbl_pengguna.getColumnModel().getColumn(6).setResizable(false);
        }

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 690, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 44, Short.MAX_VALUE))
        );

        jPanel11.setBackground(new java.awt.Color(153, 153, 153));

        TvSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TvSearchActionPerformed(evt);
            }
        });
        TvSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TvSearchKeyReleased(evt);
            }
        });

        jLabel12.setText("Search");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(TvSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TvSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 49, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Pengguna", jPanel2);

        jLabel13.setText("Kd. Tarif");

        jLabel14.setText("Daya");

        jLabel15.setText("Tarif/Kwh");

        jLabel19.setText("Denda");

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15)
                    .addComponent(jLabel19))
                .addGap(13, 13, 13)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtDaya, javax.swing.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)
                    .addComponent(txtKd, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtKwh, javax.swing.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)
                    .addComponent(txtDenda))
                .addContainerGap(113, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addComponent(txtKd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14)
                    .addComponent(txtDaya, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15)
                    .addComponent(txtKwh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(txtDenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btnInsert.setLabel("Insert");
        btnInsert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertActionPerformed(evt);
            }
        });

        btnUpdate.setLabel("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnDelete.setLabel("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnCancel.setLabel("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addContainerGap(157, Short.MAX_VALUE)
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
                    .addComponent(btnInsert, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(148, 148, 148))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnInsert, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 13, Short.MAX_VALUE))
        );

        tblTarif.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Kode Tarif", "Daya", "Tarif / Kwh", "Denda"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblTarif.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblTarifMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblTarif);
        if (tblTarif.getColumnModel().getColumnCount() > 0) {
            tblTarif.getColumnModel().getColumn(0).setResizable(false);
            tblTarif.getColumnModel().getColumn(1).setResizable(false);
            tblTarif.getColumnModel().getColumn(2).setResizable(false);
        }

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 812, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(146, Short.MAX_VALUE))
        );

        jPanel17.setBackground(new java.awt.Color(153, 153, 153));

        txtCariTarif.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCariTarifActionPerformed(evt);
            }
        });
        txtCariTarif.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCariTarifKeyReleased(evt);
            }
        });

        jLabel18.setText("Search");

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtCariTarif, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCariTarif, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(44, 44, 44))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Tarif", jPanel13);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 837, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 649, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtCariPelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCariPelActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_txtCariPelActionPerformed

    private void txtCariTarifActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCariTarifActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCariTarifActionPerformed

    private void TvSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TvSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TvSearchActionPerformed

    private void TvIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TvIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TvIDActionPerformed

    private void BtnInsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnInsertActionPerformed
        // TODO add your handling code here:
        addPengguna();
    }//GEN-LAST:event_BtnInsertActionPerformed

    private void tbl_penggunaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_penggunaMouseClicked
        // TODO add your handling code here:
        baris = tbl_pengguna.getSelectedRow();
        TableModel model = tbl_pengguna.getModel();
        
        TvID.setText(model.getValueAt(baris, 0).toString());
        TvNama.setText(model.getValueAt(baris, 1).toString());
        TvEmail.setText(model.getValueAt(baris, 2).toString());
        TvNoHp.setText(model.getValueAt(baris, 3).toString());
        TvUsername.setText(model.getValueAt(baris, 4).toString());
        TvPassword.setText(model.getValueAt(baris, 5).toString());
        CbHak.setSelectedItem(model.getValueAt(baris, 6).toString());
        
        BtnUpdate.setEnabled(true);
        BtnCancel.setEnabled(true);
        BtnDelete.setEnabled(true);
        TvID.setEnabled(false);
    }//GEN-LAST:event_tbl_penggunaMouseClicked

    private void BtnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelActionPerformed
        // TODO add your handling code here:
        TvID.setText("");
        TvNama.setText("");
        TvEmail.setText("");
        TvNoHp.setText("");
        TvUsername.setText("");
        TvPassword.setText("");
        CbHak.setSelectedItem("");
        
        TvID.setEnabled(true);
        BtnUpdate.setEnabled(false);
        BtnCancel.setEnabled(false);
        BtnDelete.setEnabled(false);
    }//GEN-LAST:event_BtnCancelActionPerformed

    private void TvSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TvSearchKeyReleased
        // TODO add your handling code here:
        String query = TvSearch.getText();
        searchData(query);
    }//GEN-LAST:event_TvSearchKeyReleased

    private void BtnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnUpdateActionPerformed
        // TODO add your handling code here:
        editPengguna();
    }//GEN-LAST:event_BtnUpdateActionPerformed

    private void BtnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDeleteActionPerformed
        // TODO add your handling code here:
        deletePengguna();
    }//GEN-LAST:event_BtnDeleteActionPerformed

    private void btnCancelPelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelPelActionPerformed
        // TODO add your handling code here:
        txtIdPel.setText("");
        txtNo.setText("");
        txtNama.setText("");
        txtAlamat.setText("");
        cbKdTarif.getSelectedItem();
        
        txtIdPel.setEnabled(true);
        btnUpdatePel.setEnabled(false);
        btnCancelPel.setEnabled(false);
        btnDeletePel.setEnabled(false);
    }//GEN-LAST:event_btnCancelPelActionPerformed

    private void btnInsertPelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertPelActionPerformed
        // TODO add your handling code here:
        addPelanggan();
    }//GEN-LAST:event_btnInsertPelActionPerformed

    private void btnUpdatePelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdatePelActionPerformed
        // TODO add your handling code here:
        editPelanggan();
    }//GEN-LAST:event_btnUpdatePelActionPerformed

    private void btnDeletePelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeletePelActionPerformed
        // TODO add your handling code here:
        deletePelanggan();
    }//GEN-LAST:event_btnDeletePelActionPerformed

    private void tblPelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPelMouseClicked
        // TODO add your handling code here:
        baris = tblPel.getSelectedRow();
        TableModel model = tblPel.getModel();
        
        txtIdPel.setText(model.getValueAt(baris, 0).toString());
        txtNo.setText(model.getValueAt(baris, 1).toString());
        txtNama.setText(model.getValueAt(baris, 2).toString());
        txtAlamat.setText(model.getValueAt(baris, 3).toString());
        cbKdTarif.setSelectedItem(model.getValueAt(baris, 4).toString());
        
        btnUpdatePel.setEnabled(true);
        btnCancelPel.setEnabled(true);
        btnDeletePel.setEnabled(true);
        txtIdPel.setEnabled(false);
    }//GEN-LAST:event_tblPelMouseClicked

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        editTarif();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        deleteTarif();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnInsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertActionPerformed
        // TODO add your handling code here:
        addTarif();
    }//GEN-LAST:event_btnInsertActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        // TODO add your handling code here:
        txtKd.setText("");
        txtDaya.setText("");
        txtKwh.setText("");
        txtDenda.setText("");
                
        txtKd.setEnabled(true);
        btnUpdate.setEnabled(false);
        btnCancel.setEnabled(false);
        btnDelete.setEnabled(false);
    }//GEN-LAST:event_btnCancelActionPerformed

    private void txtCariPelKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCariPelKeyReleased
        // TODO add your handling code here:
        String query = txtCariPel.getText();
        searchDataPel(query);
    }//GEN-LAST:event_txtCariPelKeyReleased

    private void txtCariTarifKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCariTarifKeyReleased
        // TODO add your handling code here:
        String query = txtCariTarif.getText();
        searchDataTarif(query);
    }//GEN-LAST:event_txtCariTarifKeyReleased

    private void tblTarifMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblTarifMouseClicked
        // TODO add your handling code here:
        baris = tblPel.getSelectedRow();
        TableModel model = tblPel.getModel();
        
        txtKd.setText(model.getValueAt(baris, 0).toString());
        txtDaya.setText(model.getValueAt(baris, 1).toString());
        txtKwh.setText(model.getValueAt(baris, 2).toString());
        txtDenda.setText(model.getValueAt(baris, 3).toString());
        
        btnUpdate.setEnabled(true);
        btnCancel.setEnabled(true);
        btnDelete.setEnabled(true);
        txtKd.setEnabled(false);
    }//GEN-LAST:event_tblTarifMouseClicked

    private void searchDataPel(String query){
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(dtm);
        tblPel.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(query));
    }
    
    private void searchDataTarif(String query){
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(dtm);
        tblTarif.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(query));
    }
    
    private void searchData(String query){
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(dtm);
        tbl_pengguna.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(query));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Button BtnCancel;
    private java.awt.Button BtnDelete;
    private java.awt.Button BtnInsert;
    private java.awt.Button BtnUpdate;
    private javax.swing.JComboBox<String> CbHak;
    private java.awt.TextField TvEmail;
    private java.awt.TextField TvID;
    private java.awt.TextField TvNama;
    private java.awt.TextField TvNoHp;
    private javax.swing.JPasswordField TvPassword;
    private javax.swing.JTextField TvSearch;
    private java.awt.TextField TvUsername;
    private java.awt.Button btnCancel;
    private java.awt.Button btnCancelPel;
    private java.awt.Button btnDelete;
    private java.awt.Button btnDeletePel;
    private java.awt.Button btnInsert;
    private java.awt.Button btnInsertPel;
    private java.awt.Button btnUpdate;
    private java.awt.Button btnUpdatePel;
    private javax.swing.JComboBox<String> cbKdTarif;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable tblPel;
    private javax.swing.JTable tblTarif;
    private javax.swing.JTable tbl_pengguna;
    private java.awt.TextArea txtAlamat;
    private javax.swing.JTextField txtCariPel;
    private javax.swing.JTextField txtCariTarif;
    private java.awt.TextField txtDaya;
    private javax.swing.JTextField txtDenda;
    private java.awt.TextField txtIdPel;
    private java.awt.TextField txtKd;
    private java.awt.TextField txtKwh;
    private java.awt.TextField txtNama;
    private java.awt.TextField txtNo;
    // End of variables declaration//GEN-END:variables
}
